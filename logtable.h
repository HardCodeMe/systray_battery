#ifndef LOGTABLE_H
#define LOGTABLE_H

#include <QMainWindow>
#include <QStringList>
#include "journal.h"

namespace Ui {
class LogTable;
}

class LogTable : public QMainWindow
{
    Q_OBJECT

public:
    LogTable(journal &jrnl, QWidget *parent = nullptr);
    ~LogTable();

private:
    Ui::LogTable *ui;

    void createUI(journal &jrnl);
};

#endif // LOGTABLE_H
