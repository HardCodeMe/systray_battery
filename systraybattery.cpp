#include "systraybattery.h"
#include "ui_systraybattery.h"

#include <QtWidgets>
#include <QVector>
#include <QDateTime>
#include <QDBusConnection>
#include <QDBusError>
#include "dialoglog.h"
#include "eventfilter.h"
#include <QCoreApplication>


#pragma pack(push,1)

/* Report ID 1,2 Charger state type definition */
typedef struct {
    uint8_t report_id;
    int16_t Vbat;
    int16_t Vin;
    int16_t Vout;
    int16_t Iin;
    int16_t Ibat;
    int16_t die_temp;
    int16_t bat_temp;
    int16_t loadstate;
} report_charger_state_t;

/* Report ID 3,4 Monitor state type definition */
typedef struct {
    uint8_t report_id;
    int16_t cell_voltage[6];
    int16_t temperature;
    union {
        struct {
            uint16_t               : 1;
            uint16_t               : 1;
            uint16_t               : 1;
            uint16_t balance_state : 1;
            uint16_t balance_cell1 : 2;
            uint16_t balance_cell2 : 2;
            uint16_t balance_cell3 : 2;
            uint16_t balance_cell4 : 2;
            uint16_t balance_cell5 : 2;
            uint16_t balance_cell6 : 2;
        };
        uint16_t balance;
    };
    int32_t qoulombs;
    int32_t qoulombs_max;
} report_monitor_state_t;

/* Report ID 5 Statuses */
typedef struct {
    uint8_t report_id;
    uint8_t charger1_state;         /* 1 - Ok, 0 - Error */
    uint8_t charger2_state;         /* 1 - Ok, 0 - Error */
    uint8_t monitor1_state;         /* 1 - Ok, 0 - Error */
    uint8_t monitor2_state;         /* 1 - Ok, 0 - Error */
    uint8_t battery1_connection;    /* 1 - connect, 0 - disconnect */
    uint8_t battery2_connection;    /* 1 - connect, 0 - disconnect */
    uint8_t vin_state;              /* 1 - present, 0 - absent */
} report_status_t;


/* Report ID 6 External Power Source */
typedef struct {
    uint8_t report_id;
    uint16_t Vin;
    int16_t Iin;
} report_pwrsrc_t;


/* Report ID type definition */
typedef enum {
    REPORT_ID_CHARGER1_STATE = 1,
    REPORT_ID_CHARGER2_STATE = 2,
    REPORT_ID_MONITOR1_STATE = 3,
    REPORT_ID_MONITOR2_STATE = 4,
    REPORT_ID_STATUS         = 5,
    REPORT_ID_PWRSRC_STATE   = 6
} report_id_t;

/* Charger 1 state report declaration */
static report_charger_state_t charger1_state_report = {
    .report_id = REPORT_ID_CHARGER1_STATE,
};


/* Charger 2 state report declaration */
static report_charger_state_t charger2_state_report = {
    .report_id = REPORT_ID_CHARGER2_STATE,
};


/* Monitor 1 state report declaration */
static report_monitor_state_t monitor1_state_report = {
    .report_id = REPORT_ID_MONITOR1_STATE
};


/* Monitor 2 state report declaration */
static report_monitor_state_t monitor2_state_report = {
    .report_id = REPORT_ID_MONITOR2_STATE
};

/* Status report declaration */
static report_status_t status_report = {
    .report_id = REPORT_ID_STATUS
};

/* External Power Source report declaration */
static report_pwrsrc_t pwrsrc_report = {
    .report_id = REPORT_ID_PWRSRC_STATE,
    .Vin       = 0,
    .Iin       = 0
};

#pragma pack(pop)



typedef struct {
    double voltage;
    quint16 capacity;
} acc_v_t;

/*
QVector<acc_v_t> discharge_graph = {
    {2.5,    2500},
    {2.628,  2495},
    {2.728,  2474},
    {2.808,  2454},
    {2.871,  2433},
    {2.923,  2411},
    {2.965,  2391},
    {2.999,  2370},
    {3.025,  2349},
    {3.044,  2328},
    {3.059,  2307},
    {3.07,   2287},
    {3.079, 2266},
    {3.086, 2245},
    {3.092, 2224},
    {3.097, 2203},
    {3.102, 2182},
    {3.107, 2161},
    {3.111, 2140},
    {3.116, 2120},
    {3.121, 2099},
    {3.125, 2078},
    {3.129, 2057},
    {3.133, 2037},
    {3.137, 2016},
    {3.14,  1995},
    {3.143, 1974},
    {3.147, 1953},
    {3.15,  1932},
    {3.153, 1911},
    {3.156, 1891},
    {3.158, 1870},
    {3.161, 1849},
    {3.164, 1828},
    {3.166, 1808},
    {3.168, 1787},
    {3.17,  1766},
    {3.172, 1745},
    {3.175, 1724},
    {3.176, 1704},
    {3.178, 1683},
    {3.18,  1663},
    {3.182, 1642},
    {3.184, 1620},
    {3.186, 1600},
    {3.187, 1579},
    {3.189, 1558},
    {3.19,  1538},
    {3.192, 1517},
    {3.194, 1496},
    {3.195, 1475},
    {3.196, 1454},
    {3.198, 1433},
    {3.199, 1413},
    {3.2,   1392},
    {3.201, 1370},
    {3.202, 1349},
    {3.204, 1329},
    {3.205, 1308},
    {3.206, 1287},
    {3.207, 1266},
    {3.208, 1245},
    {3.209, 1224},
    {3.21,  1204},
    {3.211, 1183},
    {3.212, 1162},
    {3.213, 1141},
    {3.214, 1100},
    {3.215, 1079},
    {3.216, 1058},
    {3.217, 1037},
    {3.218, 1016},
    {3.219,  975},
    {3.22,   954},
    {3.221,  933},
    {3.222,  913},
    {3.223,  892},
    {3.224,  850},
    {3.225,  829},
    {3.226,  808},
    {3.227,  788},
    {3.228,  767},
    {3.229,  746},
    {3.23,   725},
    {3.231,  704},
    {3.233,  683},
    {3.234,  663},
    {3.235,  642},
    {3.236,  621},
    {3.237,  600},
    {3.238,  579},
    {3.24,   558},
    {3.241,  538},
    {3.242,  517},
    {3.243,  496},
    {3.245,  475},
    {3.246,  454},
    {3.247,  433},
    {3.248,  412},
    {3.249,  392},
    {3.25,   371},
    {3.251,  350},
    {3.252,  329},
    {3.253,  308},
    {3.254,  288},
    {3.255,  267},
    {3.256,  225},
    {3.257,  200},
    {3.258,  162},
    {3.259,  141},
    {3.26,   120},
    {3.261,   79},
    {3.278,   16},
    {3.394,    0}
};



QVector<acc_v_t> charge_graph = {
    {2.5,     17},
    {2.932,   42},
    {3.026,   67},
    {3.092,   92},
    {3.141,  117},
    {3.182,  142},
    {3.215,  167},
    {3.241,  192},
    {3.253,  217},
    {3.257,  242},
    {3.261,  267},
    {3.266,  292},
    {3.27,   317},
    {3.274,  342},
    {3.278,  367},
    {3.283,  392},
    {3.288,  417},
    {3.294,  442},
    {3.299,  467},
    {3.304,  492},
    {3.309,  517},
    {3.314,  542},
    {3.319,  567},
    {3.324,  592},
    {3.328,  617},
    {3.332,  642},
    {3.335,  667},
    {3.338,  692},
    {3.341,  717},
    {3.344,  742},
    {3.345,  767},
    {3.346,  792},
    {3.347,  817},
    {3.348,  842},
    {3.349,  867},
    {3.35,   892},
    {3.351,  917},
    {3.352,  942},
    {3.353,  967},
    {3.354,  992},
    {3.355, 1017},
    {3.356, 1042},
    {3.357, 1067},
    {3.358, 1092},
    {3.359, 1117},
    {3.36,  1142},
    {3.361, 1167},
    {3.362, 1192},
    {3.363, 1217},
    {3.364, 1242},
    {3.365, 1267},
    {3.367, 1292},
    {3.368, 1317},
    {3.369, 1342},
    {3.37,  1367},
    {3.371, 1392},
    {3.372, 1417},
    {3.374, 1442},
    {3.375, 1467},
    {3.376, 1492},
    {3.377, 1517},
    {3.379, 1542},
    {3.38,  1567},
    {3.382, 1592},
    {3.383, 1617},
    {3.384, 1642},
    {3.385, 1667},
    {3.386, 1692},
    {3.387, 1717},
    {3.388, 1742},
    {3.39,  1767},
    {3.391, 1792},
    {3.393, 1817},
    {3.394, 1842},
    {3.395, 1867},
    {3.397, 1892},
    {3.398, 1917},
    {3.4  , 1942},
    {3.402, 1967},
    {3.404, 1992},
    {3.406, 2017},
    {3.408, 2042},
    {3.411, 2066},
    {3.413, 2092},
    {3.416, 2117},
    {3.419, 2142},
    {3.423, 2167},
    {3.427, 2192},
    {3.432, 2217},
    {3.438, 2242},
    {3.444, 2267},
    {3.452, 2292},
    {3.463, 2317},
    {3.478, 2342},
    {3.498, 2367},
    {3.53,  2392},
    {3.581, 2417},
    {3.6,   2437}
};
*/

SysTrayBattery::SysTrayBattery(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::SysTrayBattery)
{
    ui->setupUi(this);

    //this->setWindowFlags(Qt::FramelessWindowHint);
    //this->setAttribute(Qt::WA_TranslucentBackground);

    setFixedSize(width(), height());

    Qt::WindowFlags flags = windowFlags();
    Qt::WindowFlags helpFlag = Qt::WindowContextHelpButtonHint;
    flags = flags & (~helpFlag);
    setWindowFlags(flags);

    qApp->installEventFilter(this);

    QAction* actionQuit = new QAction(tr("&Quit"), this);
    connect(actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
    /*
    QAction* actionRestore = new QAction(tr("&Restore"), this);
    connect(actionRestore, SIGNAL(triggered()), this, SLOT(showNormal()));
    QAction* actionMinimize = new QAction(tr("Mi&nimize"), this);
    connect(actionMinimize, SIGNAL(triggered()), this, SLOT(hide()));
    */
    QAction* actionAbout = new QAction(tr("&About"), this);
    //connect(actionAbout, SIGNAL(triggered()), this, SLOT(showNormal()));

    m_ptrayIconMenu = new QMenu(this);
  /*m_ptrayIconMenu->addAction(actionRestore);
    m_ptrayIconMenu->addAction(actionMinimize);*/
    m_ptrayIconMenu->addAction(actionAbout);
    m_ptrayIconMenu->addSeparator();
    m_ptrayIconMenu->addAction(actionQuit);

    m_ptrayIcon1 = new QSystemTrayIcon(this);
    m_ptrayIcon1->setContextMenu(m_ptrayIconMenu);
    m_ptrayIcon1->setToolTip("Left battery disconnected");

    m_ptrayIcon1->setIcon(QPixmap(":/icons/battery_absent.png"));
    m_ptrayIcon1->show();

    m_ptrayIcon2 = new QSystemTrayIcon(this);
    m_ptrayIcon2->setContextMenu(m_ptrayIconMenu);
    m_ptrayIcon2->setToolTip("Right battery disconnected");

    m_ptrayIcon2->setIcon(QPixmap(":/icons/battery_absent.png"));
    m_ptrayIcon2->show();

    connect(m_ptrayIcon1, &QSystemTrayIcon::activated, this, &SysTrayBattery::iconActivated);
    connect(m_ptrayIcon2, &QSystemTrayIcon::activated, this, &SysTrayBattery::iconActivated);

    int res;

    ui->checkBox->setCheckState(Qt::Checked);

    //QAbstractEventDispatcher::instance()->installNativeEventFilter(new MyEventFilter);

    showMinimized();
    hide();

    //qDebug() << QDBusConnection::systemBus().connect("org.freedesktop.UPower", "/org/freedesktop/UPower", "org.freedesktop.UPower", "Sleeping", this,  SLOT(SystemIsGoingToSuspend()));
    //qDebug() << QDBusConnection::systemBus().connect("org.freedesktop.UPower", "/org/freedesktop/UPower", "org.freedesktop.UPower", "Resuming", this,  SLOT(SystemIsGoingToResume()));
    //qDebug() << QDBusConnection::systemBus().lastError().message();

    // Initialize the hidapi library
    res = hid_init();

    if (res) {
        qDebug() << "Error hid initializes" << endl;
    }

    // Open the device using the VID, PID,
    // and optionally the Serial number.
    handle = hid_open(0x0483, 0x5750, NULL);

    /* Run device polling timer to update state every 1 second */
    QTimer *ptmr_stat = new QTimer;
    ptmr_stat->setInterval(1000);
    connect(ptmr_stat, SIGNAL(timeout()), SLOT(slotDevice_polling()));
    ptmr_stat->start();
}

SysTrayBattery::~SysTrayBattery()
{
    delete ui;
}

void SysTrayBattery::slotDevice_polling()
{
    int res;
    quint8 bat1_Q_percentage;
    quint8 bat2_Q_percentage;
    QDateTime datetime = QDateTime::currentDateTime();
    journal::jline_t jline;

#define GROUP_BOX_ERROR_STYLE             \
   "QGroupBox {                          "\
   "    border: 2px solid rgb(255, 0, 0);"\
   "    margin-top: 6px;                 "\
   "    padding-top: 8px;                "\
   "}                                    "\
   "                                     "\
   "QGroupBox::title {                   "\
   "    subcontrol-origin: margin;       "\
   "    subcontrol-position: top left;   "\
   "    left: 5px;                       "\
   "    margin: 0 5px;                   "\
   "}                                    "\

#define GROUP_BOX_OK_STYLE                \
   "QGroupBox {                          "\
   "    border: 2px solid rgb(0, 170, 0);"\
   "    margin-top: 6px;                 "\
   "    padding-top: 8px;                "\
   "}                                    "\
   "                                     "\
   "QGroupBox::title {                   "\
   "    subcontrol-origin: margin;       "\
   "    subcontrol-position: top left;   "\
   "    left: 5px;                       "\
   "    margin: 0 5px;                   "\
   "}                                    "\

#define CHR_ERROR_INDICATE(n_chr) \
({\
    ui->lcdNumber_chr##n_chr##_Vbat->display(0);     \
    ui->lcdNumber_chr##n_chr##_Vin->display(0);      \
    ui->lcdNumber_chr##n_chr##_Vout->display(0);     \
    ui->lcdNumber_chr##n_chr##_Iin->display(0);      \
    ui->lcdNumber_chr##n_chr##_Ibat->display(0);     \
    ui->lcdNumber_chr##n_chr##_temp->display(0);     \
    ui->lcdNumber_chr##n_chr##_temp_bat->display(0); \
    ui->label_ch##n_chr##_loadstate->setText("OFF"); \
    ui->groupBox_chr##n_chr->setStyleSheet(GROUP_BOX_ERROR_STYLE);      \
    ui->label_chr##n_chr##_fatal_error->show();                         \
    m_ptrayIcon##n_chr->setToolTip("Error communication");              \
    m_ptrayIcon##n_chr->setIcon(QPixmap(":/icons/battery_absent.png")); \
})

#define CHR_DATA_INDICATE(n_chr) \
({\
    ui->lcdNumber_chr##n_chr##_Vbat->display(charger##n_chr##_state_report.Vbat);         \
    ui->lcdNumber_chr##n_chr##_Vin->display(charger##n_chr##_state_report.Vin);           \
    ui->lcdNumber_chr##n_chr##_Vout->display(charger##n_chr##_state_report.Vout);         \
    ui->lcdNumber_chr##n_chr##_Iin->display(charger##n_chr##_state_report.Iin);           \
    ui->lcdNumber_chr##n_chr##_Ibat->display(charger##n_chr##_state_report.Ibat);         \
    ui->lcdNumber_chr##n_chr##_temp->display(charger##n_chr##_state_report.die_temp);     \
    ui->lcdNumber_chr##n_chr##_temp_bat->display(charger##n_chr##_state_report.bat_temp); \
    ui->label_ch##n_chr##_loadstate->setText(((charger##n_chr##_state_report.loadstate) ? "ON" : "OFF")); \
    ui->groupBox_chr##n_chr->setStyleSheet(GROUP_BOX_OK_STYLE);                           \
    ui->label_chr##n_chr##_fatal_error->hide();                                           \
})

#define BAT_ERROR_INDICATE(n_bat) \
({\
    ui->lcdNumber_bat##n_bat##_Vcell1->display(0);  \
    ui->lcdNumber_bat##n_bat##_Vcell2->display(0);  \
    ui->lcdNumber_bat##n_bat##_Vcell3->display(0);  \
    ui->lcdNumber_bat##n_bat##_Vcell4->display(0);  \
    ui->lcdNumber_bat##n_bat##_Vcell5->display(0);  \
    ui->lcdNumber_bat##n_bat##_Vcell6->display(0);  \
    ui->lcdNumber_bat##n_bat##_temp->display(0);    \
    ui->lcdNumber_bat##n_bat##_Q->display(0);       \
    ui->lcdNumber_bat##n_bat##_Qmax->display(0);    \
    ui->lcdNumber_bat##n_bat##_Q_per->display(0);   \
    ui->groupBox_bat##n_bat->setStyleSheet(GROUP_BOX_ERROR_STYLE);                          \
    /*ui->label_bat##n_bat##_absent_battery->show();*/                                      \
    if (status_report.battery##n_bat##_connection && !status_report.monitor##n_bat##_state) \
        m_ptrayIcon##n_bat->setToolTip("Error communication");                              \
    m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_absent.png"));                     \
    ui->label_bat##n_bat##_balance_cell1->hide();                                           \
    ui->label_bat##n_bat##_balance_cell2->hide();                                           \
    ui->label_bat##n_bat##_balance_cell3->hide();                                           \
    ui->label_bat##n_bat##_balance_cell4->hide();                                           \
    ui->label_bat##n_bat##_balance_cell5->hide();                                           \
    ui->label_bat##n_bat##_balance_cell6->hide();                                           \
})

#define BAT_BALANCE_INDICATE(n_bat, n_cell) \
({\
    if (monitor##n_bat##_state_report.balance_cell##n_cell > 0) {                                      \
        if (monitor##n_bat##_state_report.balance_cell##n_cell == 3) {                                 \
            ui->label_bat##n_bat##_balance_cell##n_cell->setPixmap(QPixmap(":/icons/arrow-up.png"));   \
        }                                                                                              \
        else {                                                                                         \
            ui->label_bat##n_bat##_balance_cell##n_cell->setPixmap(QPixmap(":/icons/down-arrow.png")); \
        }                                                                                              \
        ui->label_bat##n_bat##_balance_cell##n_cell->show();                                           \
    }                                                                                                  \
    else                                                                                               \
        ui->label_bat##n_bat##_balance_cell##n_cell->hide();                                           \
})

#define BAT_DATA_INDICATE(n_bat) \
({\
    ui->lcdNumber_bat##n_bat##_Vcell1->display(monitor##n_bat##_state_report.cell_voltage[0]); \
    ui->lcdNumber_bat##n_bat##_Vcell2->display(monitor##n_bat##_state_report.cell_voltage[1]); \
    ui->lcdNumber_bat##n_bat##_Vcell3->display(monitor##n_bat##_state_report.cell_voltage[2]); \
    ui->lcdNumber_bat##n_bat##_Vcell4->display(monitor##n_bat##_state_report.cell_voltage[3]); \
    ui->lcdNumber_bat##n_bat##_Vcell5->display(monitor##n_bat##_state_report.cell_voltage[4]); \
    ui->lcdNumber_bat##n_bat##_Vcell6->display(monitor##n_bat##_state_report.cell_voltage[5]); \
    ui->lcdNumber_bat##n_bat##_temp->display(monitor##n_bat##_state_report.temperature);       \
    ui->lcdNumber_bat##n_bat##_Q->display(monitor##n_bat##_state_report.qoulombs);             \
    ui->lcdNumber_bat##n_bat##_Qmax->display(monitor##n_bat##_state_report.qoulombs_max);      \
    ui->lcdNumber_bat##n_bat##_Q_per->display(bat##n_bat##_Q_percentage);                      \
    ui->groupBox_bat##n_bat->setStyleSheet(GROUP_BOX_OK_STYLE);                                     \
    /*ui->label_bat##n_bat##_absent_battery->hide();*/                                              \
    if (status_report.battery##n_bat##_connection && monitor##n_bat##_state_report.balance_state) { \
        BAT_BALANCE_INDICATE(n_bat, 1);                                                             \
        BAT_BALANCE_INDICATE(n_bat, 2);                                                             \
        BAT_BALANCE_INDICATE(n_bat, 3);                                                             \
        BAT_BALANCE_INDICATE(n_bat, 4);                                                             \
        BAT_BALANCE_INDICATE(n_bat, 5);                                                             \
        BAT_BALANCE_INDICATE(n_bat, 6);                                                             \
    }                                                                                               \
    else {                                                                                          \
        ui->label_bat##n_bat##_balance_cell1->hide();                                               \
        ui->label_bat##n_bat##_balance_cell2->hide();                                               \
        ui->label_bat##n_bat##_balance_cell3->hide();                                               \
        ui->label_bat##n_bat##_balance_cell4->hide();                                               \
        ui->label_bat##n_bat##_balance_cell5->hide();                                               \
        ui->label_bat##n_bat##_balance_cell6->hide();                                               \
    }                                                                                               \
})



#define BATTERY_ICON_INDICATE(n_bat)\
({\
    if (status_report.vin_state) {                                                          \
        bat##n_bat##_discharged_message_viewed = false;                                     \
        /* Battery Charges */                                                               \
        if (bat##n_bat##_Q_percentage < 10) {                                               \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_0.png"));         \
        } else if (bat##n_bat##_Q_percentage >= 10 && bat##n_bat##_Q_percentage < 25) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_10.png"));        \
        } else if (bat##n_bat##_Q_percentage >= 25 && bat##n_bat##_Q_percentage < 40) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_25.png"));        \
        } else if (bat##n_bat##_Q_percentage >= 40 && bat##n_bat##_Q_percentage < 50) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_40.png"));        \
        } else if (bat##n_bat##_Q_percentage >= 50 && bat##n_bat##_Q_percentage < 60) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_50.png"));        \
        } else if (bat##n_bat##_Q_percentage >= 60 && bat##n_bat##_Q_percentage < 75) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_60.png"));        \
        } else if (bat##n_bat##_Q_percentage >= 75 && bat##n_bat##_Q_percentage < 80) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_75.png"));        \
        } else if (bat##n_bat##_Q_percentage >= 80 && bat##n_bat##_Q_percentage < 100) {    \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_80.png"));        \
        } else if (bat##n_bat##_Q_percentage == 100) {                                      \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_charging_100.png"));       \
        }                                                                                   \
    } else {                                                                                \
        /* Battery Discharges */                                                            \
        if (bat##n_bat##_Q_percentage < 10) {                                               \
            if (bat##n_bat##_discharged_message_viewed == false) {                          \
                m_ptrayIcon##n_bat->showMessage("Battery discharged", "Less than 10 percent battery power left", QSystemTrayIcon::Critical); \
                bat##n_bat##_discharged_message_viewed = true;                              \
            }                                                                               \
            if (battery##n_bat##_empty_icon_state)                                          \
                m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_0.png"));              \
            else                                                                            \
                m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_absent.png"));         \
            battery##n_bat##_empty_icon_state ^= 1;                                         \
        } else if (bat##n_bat##_Q_percentage >= 10 && bat##n_bat##_Q_percentage < 25) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_10.png"));                 \
        } else if (bat##n_bat##_Q_percentage >= 25 && bat##n_bat##_Q_percentage < 40) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_25.png"));                 \
        } else if (bat##n_bat##_Q_percentage >= 40 && bat##n_bat##_Q_percentage < 50) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_40.png"));                 \
        } else if (bat##n_bat##_Q_percentage >= 50 && bat##n_bat##_Q_percentage < 60) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_50.png"));                 \
        } else if (bat##n_bat##_Q_percentage >= 60 && bat##n_bat##_Q_percentage < 75) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_60.png"));                 \
        } else if (bat##n_bat##_Q_percentage >= 75 && bat##n_bat##_Q_percentage < 80) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_75.png"));                 \
        } else if (bat##n_bat##_Q_percentage >= 80 && bat##n_bat##_Q_percentage < 90) {     \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_80.png"));                 \
        } else if (bat##n_bat##_Q_percentage >= 90 && bat##n_bat##_Q_percentage <= 100) {   \
            m_ptrayIcon##n_bat->setIcon(QPixmap(":/icons/battery_100.png"));                \
        }                                                                                   \
    }                                                                                       \
})


#define BYTEBCDTOHEX(bcdbyte) \
    (((bcdbyte) & 0xf) + (((bcdbyte) >> 4) & 0xf) * 10)

#define BYTEHEXTOBCD(bcdbyte) \
    (((((bcdbyte) / 10) << 4) & 0xf0) + (((bcdbyte) % 10) & 0xf))

    memset(&jline, 0xff, sizeof(jline));
    jline.datetime.year  = BYTEHEXTOBCD(datetime.date().year() - 2000);
    jline.datetime.month = BYTEHEXTOBCD(datetime.date().month());
    jline.datetime.day   = BYTEHEXTOBCD(datetime.date().day());
    jline.datetime.hour  = BYTEHEXTOBCD(datetime.time().hour());
    jline.datetime.min   = BYTEHEXTOBCD(datetime.time().minute());
    jline.datetime.sec   = BYTEHEXTOBCD(datetime.time().second());

    if (handle == NULL) {
        // Open the device using the VID, PID,
        // and optionally the Serial number.
        handle = hid_open(0x0483, 0x5750, NULL);
    }

    if (handle) {

        res = hid_get_feature_report(handle, (quint8 *)&pwrsrc_report, sizeof(pwrsrc_report));
        if (res == -1) {
            ui->groupBox_pwrsrc->setStyleSheet(GROUP_BOX_ERROR_STYLE);
            ui->lcdNumber_extpwr_Vin->display(0);
            ui->lcdNumber_extpwr_Iin->display(0);
            hid_close(handle);
            handle = NULL;
            CHR_ERROR_INDICATE(1);
            CHR_ERROR_INDICATE(2);
            BAT_ERROR_INDICATE(1);
            BAT_ERROR_INDICATE(2);
            return;
        }
        else {
            if (pwrsrc_report.Vin > 8500) {
                ui->groupBox_pwrsrc->setStyleSheet(GROUP_BOX_OK_STYLE);
            } else {
                ui->groupBox_pwrsrc->setStyleSheet(GROUP_BOX_ERROR_STYLE);
            }

            ui->lcdNumber_extpwr_Vin->display(pwrsrc_report.Vin);
            ui->lcdNumber_extpwr_Iin->display(pwrsrc_report.Iin);
            jline.pwrsrc.Iin = pwrsrc_report.Iin;
            jline.pwrsrc.Vin = pwrsrc_report.Vin;
        }

        res = hid_get_feature_report(handle, (quint8 *)&status_report, sizeof(status_report));
        if (res != -1) {
            if (status_report.charger1_state) {
                res = hid_get_feature_report(handle, (quint8 *)&charger1_state_report, sizeof(charger1_state_report));
                if (res != -1) {
                    CHR_DATA_INDICATE(1);
                    jline.charger1.Vin      = charger1_state_report.Vin;
                    jline.charger1.Iin      = charger1_state_report.Iin;
                    jline.charger1.Vout     = charger1_state_report.Vout;
                    jline.charger1.Ibat     = charger1_state_report.Ibat;
                    jline.charger1.Vbat     = charger1_state_report.Vbat;
                    jline.charger1.die_temp = charger1_state_report.die_temp;
                    jline.charger1.bat_temp = charger1_state_report.bat_temp;
                } else {
                    CHR_ERROR_INDICATE(1);
                }
            } else {
                CHR_ERROR_INDICATE(1);
            }

            if (status_report.charger2_state) {
                res = hid_get_feature_report(handle, (quint8 *)&charger2_state_report, sizeof(charger2_state_report));
                if (res != -1) {
                    CHR_DATA_INDICATE(2);
                    jline.charger2.Vin      = charger2_state_report.Vin;
                    jline.charger2.Iin      = charger2_state_report.Iin;
                    jline.charger2.Vout     = charger2_state_report.Vout;
                    jline.charger2.Ibat     = charger2_state_report.Ibat;
                    jline.charger2.Vbat     = charger2_state_report.Vbat;
                    jline.charger2.die_temp = charger2_state_report.die_temp;
                    jline.charger2.bat_temp = charger2_state_report.bat_temp;
                } else {
                    CHR_ERROR_INDICATE(2);
                }
            } else {
                CHR_ERROR_INDICATE(2);
            }

            if (status_report.battery1_connection && status_report.monitor1_state) {
                res = hid_get_feature_report(handle, (quint8 *)&monitor1_state_report, sizeof(monitor1_state_report));
                if (res != -1) {
                    bat1_Q_percentage = get_battery_qoulombs_percentage(monitor1_state_report.qoulombs, monitor1_state_report.qoulombs_max);
                    //bat1_charge_percentage = get_battery_charge_percentage(charger1_state_report.Vbat, status_report.vin_state);
                    //bat1_discharge_percentage = get_battery_discharge_percentage(charger1_state_report.Vbat, status_report.vin_state);
                    m_ptrayIcon1->setToolTip("Left battery " + QString::number(bat1_Q_percentage) + "%");

                    BATTERY_ICON_INDICATE(1);

                    BAT_DATA_INDICATE(1);
                    jline.monitor1.cell_voltage[0] = monitor1_state_report.cell_voltage[0];
                    jline.monitor1.cell_voltage[1] = monitor1_state_report.cell_voltage[1];
                    jline.monitor1.cell_voltage[2] = monitor1_state_report.cell_voltage[2];
                    jline.monitor1.cell_voltage[3] = monitor1_state_report.cell_voltage[3];
                    jline.monitor1.cell_voltage[4] = monitor1_state_report.cell_voltage[4];
                    jline.monitor1.cell_voltage[5] = monitor1_state_report.cell_voltage[5];
                    jline.monitor1.temperature     = monitor1_state_report.temperature;
                    jline.monitor1.qoulombs        = monitor1_state_report.qoulombs;
                    jline.monitor1.qoulombs_max    = monitor1_state_report.qoulombs_max;
                } else {
                    BAT_ERROR_INDICATE(1);
                }
            } else {
                BAT_ERROR_INDICATE(1);
                m_ptrayIcon1->setIcon(QPixmap(":/icons/battery_absent.png"));
                m_ptrayIcon1->setToolTip("Left battery disconnected");
            }

            if (status_report.battery2_connection && status_report.monitor2_state) {
                res = hid_get_feature_report(handle, (quint8 *)&monitor2_state_report, sizeof(monitor2_state_report));
                if (res != -1) {
                    bat2_Q_percentage = get_battery_qoulombs_percentage(monitor2_state_report.qoulombs, monitor2_state_report.qoulombs_max);
                    //bat2_charge_percentage = get_battery_charge_percentage(charger2_state_report.Vbat, status_report.vin_state);
                    //bat2_discharge_percentage = get_battery_discharge_percentage(charger2_state_report.Vbat, status_report.vin_state);
                    m_ptrayIcon2->setToolTip("Right battery " + QString::number(bat2_Q_percentage) + "%");

                    BATTERY_ICON_INDICATE(2);

                    BAT_DATA_INDICATE(2);

                    jline.monitor2.cell_voltage[0] = monitor2_state_report.cell_voltage[0];
                    jline.monitor2.cell_voltage[1] = monitor2_state_report.cell_voltage[1];
                    jline.monitor2.cell_voltage[2] = monitor2_state_report.cell_voltage[2];
                    jline.monitor2.cell_voltage[3] = monitor2_state_report.cell_voltage[3];
                    jline.monitor2.cell_voltage[4] = monitor2_state_report.cell_voltage[4];
                    jline.monitor2.cell_voltage[5] = monitor2_state_report.cell_voltage[5];
                    jline.monitor2.temperature     = monitor2_state_report.temperature;
                    jline.monitor2.qoulombs        = monitor2_state_report.qoulombs;
                    jline.monitor2.qoulombs_max    = monitor2_state_report.qoulombs_max;
                } else {
                    BAT_ERROR_INDICATE(2);
                }
            } else {
                BAT_ERROR_INDICATE(2);
                m_ptrayIcon2->setIcon(QPixmap(":/icons/battery_absent.png"));
                m_ptrayIcon2->setToolTip("Right battery disconnected");
            }

            if (ui->checkBox->isChecked()) {/*
                if (pwrsrc_report.Vin < 9000) {
                    if ((status_report.charger1_state && status_report.battery1_connection && status_report.monitor1_state && bat1_Q_percentage <= 7 &&
                         status_report.charger2_state && status_report.battery2_connection && status_report.monitor2_state && bat2_Q_percentage <= 7) ||
                        (status_report.charger1_state && status_report.battery1_connection && status_report.monitor1_state && bat1_Q_percentage <= 7 &&
                         (!status_report.charger2_state || !status_report.battery2_connection || !status_report.monitor2_state)) ||
                        (status_report.charger2_state && status_report.battery2_connection && status_report.monitor2_state && bat2_Q_percentage <= 7 &&
                         (!status_report.charger1_state || !status_report.battery1_connection || !status_report.monitor1_state))) {
                        #if defined(Q_OS_WIN)
                        QProcess *m_process = new QProcess(this);
                        m_process->start("shutdown /h");
                        #elif defined(Q_OS_LINUX)

                        #endif
                    }
                }*/
            }
        } else {

            hid_close(handle);
            handle = NULL;

            CHR_ERROR_INDICATE(1);
            CHR_ERROR_INDICATE(2);
            BAT_ERROR_INDICATE(1);
            BAT_ERROR_INDICATE(2);
        }
    } else {
        CHR_ERROR_INDICATE(1);
        CHR_ERROR_INDICATE(2);
        BAT_ERROR_INDICATE(1);
        BAT_ERROR_INDICATE(2);
    }


    logjournal.write_line(jline);
}

void SysTrayBattery::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    /*
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        if (isHidden()) {
            showNormal();
            activateWindow();
        }
        else
            hide();
        break;
    case QSystemTrayIcon::MiddleClick:
        break;
    default:
        ;
    }*/

    if (secret_sequence[secret_sequence_cnt] == reason) {
        secret_sequence_cnt++;
        if (secret_sequence_cnt == SECRET_SEQUENCE_LEN) {
            secret_sequence_cnt = 0;
            if (isHidden()) {
                showNormal();
                activateWindow();
            }
            else
                hide();
        }
    }
    else {
        secret_sequence_cnt = 0;
    }
}

/*virtual*/void SysTrayBattery::closeEvent(QCloseEvent* ev)
{
    if (isVisible()) {
        hide();
        ev->ignore();
    }
}
/*
quint8 SysTrayBattery::get_battery_charge_percentage(qint16 battery_voltage, bool vin_state)
{
    int i;
    quint16 cap = 0;
    double bat_vol;

    bat_vol = (double)battery_voltage / (double)6000;

    if (vin_state) {
        for (i = 0; i < charge_graph.size(); i++) {
            if (charge_graph[i].voltage == bat_vol) {
                cap = charge_graph[i].capacity;
                break;
            }
            else if (charge_graph[i].voltage > bat_vol) {
                cap = charge_graph[i - 1].capacity + ((bat_vol - charge_graph[i - 1].voltage) / ((charge_graph[i].voltage - charge_graph[i - 1].voltage) / (charge_graph[i].capacity - charge_graph[i - 1].capacity)));
                break;
            }
        }

        return (quint8)((cap * 100) / 2437);
    }
    else {
        for (i = 0; i < discharge_graph.size(); i++) {
            if (discharge_graph[i].voltage == bat_vol) {
                cap = discharge_graph[i].capacity;
                break;
            }
            else if (discharge_graph[i].voltage > bat_vol) {
                cap = discharge_graph[i - 1].capacity + ((bat_vol - discharge_graph[i - 1].voltage) / ((discharge_graph[i].voltage - discharge_graph[i - 1].voltage) / (discharge_graph[i].capacity - discharge_graph[i - 1].capacity)));
                break;
            }
        }

        return (quint8)(((2490 - cap) * 100) / 2490);
    }
}
*/

/*
quint8 SysTrayBattery::get_battery_charge_percentage(qint16 battery_voltage, bool vin_state)
{
    int i;
    quint16 cap = 0;
    double bat_vol;

    bat_vol = (double)battery_voltage / (double)6000;

    for (i = 0; i < charge_graph.size(); i++) {
        if (charge_graph[i].voltage == bat_vol) {
            cap = charge_graph[i].capacity;
            break;
        }
        else if (charge_graph[i].voltage > bat_vol) {
            cap = charge_graph[i - 1].capacity + ((bat_vol - charge_graph[i - 1].voltage) / ((charge_graph[i].voltage - charge_graph[i - 1].voltage) / (charge_graph[i].capacity - charge_graph[i - 1].capacity)));
            break;
        }
    }

    return (quint8)((cap * 100) / 2437);
}


quint8 SysTrayBattery::get_battery_discharge_percentage(qint16 battery_voltage, bool vin_state)
{
    int i;
    quint16 cap = 0;
    double bat_vol;

    bat_vol = (double)battery_voltage / (double)6000;

    for (i = 0; i < discharge_graph.size(); i++) {
        if (discharge_graph[i].voltage == bat_vol) {
            cap = discharge_graph[i].capacity;
            break;
        }
        else if (discharge_graph[i].voltage > bat_vol) {
            cap = discharge_graph[i - 1].capacity + ((bat_vol - discharge_graph[i - 1].voltage) / ((discharge_graph[i].voltage - discharge_graph[i - 1].voltage) / (discharge_graph[i].capacity - discharge_graph[i - 1].capacity)));
            break;
        }
    }

    return (quint8)(((2490 - cap) * 100) / 2490);
}
*/

quint8 SysTrayBattery::get_battery_qoulombs_percentage(int32_t qoulombs, int32_t qoulombs_max)
{
    const int32_t qoulombs_max_ = 8600000;
    quint8 percent;

    if (qoulombs_max == 0)
        qoulombs_max = qoulombs_max_;

    percent = (quint8)((qoulombs * 100) / qoulombs_max);

    if (percent > 100)
        percent = 100;

    return percent;
}

void SysTrayBattery::focusInEvent(QFocusEvent *event)
{
    QWidget::focusInEvent(event);
}

bool SysTrayBattery::eventFilter(QObject *watched, QEvent *e)
{/*
    if(((QFocusEvent*)e)->lostFocus())
    {
        for(int i=0;i < qApp->allWidgets().count();i++)
        {   if(qApp->allWidgets()[i]->hasFocus())
            return false;           //свой виджет
        }
        this->hide();               //чужой
    }*/
    return false;
}

void SysTrayBattery::on_pushButton_clicked()
{
    DialogLog dialoglog(logjournal);
    dialoglog.exec();
}

void SysTrayBattery::on_chkbox_ch1_forcedload_on_stateChanged(int arg1)
{

}

void SysTrayBattery::on_chkbox_ch2_forcedload_on_stateChanged(int arg1)
{

}

void SysTrayBattery::on_pushButton_2_clicked()
{
    journal::jline_t jline;
    QDateTime datetime = QDateTime::currentDateTime();

    //logjournal.clearall();
#define BYTEBCDTOHEX(bcdbyte) \
    (((bcdbyte) & 0xf) + (((bcdbyte) >> 4) & 0xf) * 10)

#define BYTEHEXTOBCD(bcdbyte) \
    (((((bcdbyte) / 10) << 4) & 0xf0) + (((bcdbyte) % 10) & 0xf))

    memset(&jline, 0xff, sizeof(jline));
    jline.datetime.year  = BYTEHEXTOBCD(datetime.date().year() - 2000);
    jline.datetime.month = BYTEHEXTOBCD(datetime.date().month());
    jline.datetime.day   = BYTEHEXTOBCD(datetime.date().day());
    jline.datetime.hour  = BYTEHEXTOBCD(datetime.time().hour());
    jline.datetime.min   = BYTEHEXTOBCD(datetime.time().minute());
    jline.datetime.sec   = BYTEHEXTOBCD(datetime.time().second());

    logjournal.write_line(jline);
}

void SysTrayBattery::SystemIsGoingToSuspend()
{
    m_ptrayIcon1->showMessage("System syspended", "Less than 10 percent battery power left", QSystemTrayIcon::Critical);
}

void SysTrayBattery::SystemIsGoingToResume()
{
    m_ptrayIcon1->showMessage("System resumed", "Less than 10 percent battery power left", QSystemTrayIcon::Critical);
}
