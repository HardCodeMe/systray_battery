#ifndef JOURNAL_H
#define JOURNAL_H

#include <cstdint>
#include <QFile>
#include <QDir>

#pragma pack(push,1)
class journal
{
public:
    typedef struct {
        int16_t Vin;
        int16_t Iin;
    } pwrsrc_t;

    typedef struct {
        int16_t Vbat;
        int16_t Vin;
        int16_t Vout;
        int16_t Iin;
        int16_t Ibat;
        int16_t die_temp;
        int16_t bat_temp;
    } charger_t;

    typedef struct {
        int16_t cell_voltage[6];
        int16_t temperature;
        int32_t qoulombs;
        int32_t qoulombs_max;
    } monitor_t;

    typedef struct {
        quint8 day;
        quint8 month;
        quint8 year;
        quint8 hour;
        quint8 min;
        quint8 sec;
    } datetime_t;

    typedef struct {
        datetime_t datetime;
        pwrsrc_t  pwrsrc;
        charger_t charger1;
        charger_t charger2;
        monitor_t monitor1;
        monitor_t monitor2;
    } jline_t;


    QFile   logfile;
    quint32 lines;
    quint32 idx;
    const quint32 maxlines;

    journal(quint32 maxlines);
    ~journal();
    bool write_line(jline_t &jline);
    quint32 get_lines();
    bool read_line(uint32_t nline, jline_t &jline);
    bool clearall();
};

#pragma pack(pop)

#endif // JOURNAL_H
