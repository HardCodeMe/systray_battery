#ifndef SYSTRAYBATTERY_H
#define SYSTRAYBATTERY_H

#include <QDialog>
#include <QSystemTrayIcon>
#include "libhid/include/hidapi.h"
#include <journal.h>



QT_BEGIN_NAMESPACE
namespace Ui { class SysTrayBattery; }
QT_END_NAMESPACE

class QSystemTrayIcon;
class QMenu;

class SysTrayBattery : public QDialog
{
    Q_OBJECT

private:
    QSystemTrayIcon* m_ptrayIcon1;
    QSystemTrayIcon* m_ptrayIcon2;
    QMenu*           m_ptrayIconMenu;
    hid_device*      handle;
    bool             battery1_empty_icon_state = false;
    bool             battery2_empty_icon_state = false;
    bool             bat1_discharged_message_viewed = false;
    bool             bat2_discharged_message_viewed = false;
    enum {SECRET_SEQUENCE_LEN = 3};
    const QSystemTrayIcon::ActivationReason secret_sequence[SECRET_SEQUENCE_LEN] = {QSystemTrayIcon::DoubleClick, QSystemTrayIcon::Trigger, QSystemTrayIcon::MiddleClick};
    quint32          secret_sequence_cnt = 0;
public:
    SysTrayBattery(QWidget *parent = nullptr);
    ~SysTrayBattery();
    journal logjournal{90};

private slots:
    void slotDevice_polling();
    void iconActivated(QSystemTrayIcon::ActivationReason reason);

    void on_pushButton_clicked();

    void on_chkbox_ch1_forcedload_on_stateChanged(int arg1);

    void on_chkbox_ch2_forcedload_on_stateChanged(int arg1);

    void on_pushButton_2_clicked();

    void SystemIsGoingToSuspend();
    void SystemIsGoingToResume();

protected:
    virtual void closeEvent(QCloseEvent*);

private:
    Ui::SysTrayBattery *ui;/*
    quint8 get_battery_charge_percentage(qint16 battery_voltage, bool vin_state);
    quint8 get_battery_discharge_percentage(qint16 battery_voltage, bool vin_state);*/
    quint8 get_battery_qoulombs_percentage(int32_t qoulombs, int32_t qoulombs_max);
    // QWidget interface
protected:
    virtual void focusInEvent(QFocusEvent *event);

    // QObject interface
public:
    virtual bool eventFilter(QObject *watched, QEvent *event);
};
#endif // SYSTRAYBATTERY_H
