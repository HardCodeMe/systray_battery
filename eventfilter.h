#ifndef EVENTFILTER_H
#define EVENTFILTER_H

#include <QAbstractNativeEventFilter>
#include <QAbstractEventDispatcher>
#include <QDebug>
#include <windows.h>
class MyEventFilter : public QAbstractNativeEventFilter {
 public:
  virtual bool nativeEventFilter(const QByteArray& eventType, void* message, long*) Q_DECL_OVERRIDE {
    MSG* msg = static_cast< MSG* >( message );

    if (msg->message == WM_POWERBROADCAST) {
      switch (msg->wParam) {
        case PBT_APMPOWERSTATUSCHANGE:
          qDebug() << ("PBT_APMPOWERSTATUSCHANGE  received\n");
          break;
        case PBT_APMRESUMEAUTOMATIC:
          qDebug() << ("PBT_APMRESUMEAUTOMATIC  received\n");
          break;
        case PBT_APMRESUMESUSPEND:
          qDebug() << ("PBT_APMRESUMESUSPEND  received\n");
          break;
        case PBT_APMSUSPEND:
          qDebug() << ("PBT_APMSUSPEND  received\n");
          break;
      }
    }
    return false;
  }
};

#endif // EVENTFILTER_H
