QT       += core gui dbus

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32:RC_ICONS += stc.ico

win32|win64{
LIBS += -Wl,-Bstatic #, -lboost_regex, -lgmp, -lgmpxx, -Bdynamic
QMAKE_LFLAGS += -static
}

QMAKE_LFLAGS += -static-libgcc
CONFIG += staticlib

win32|win64{
INCLUDEPATH += $$PWD/libhid/include
LIBS += -L$$PWD/libhid/lib -lhidapi -lsetupapi
}

linux {
LIBS += -ludev
}

SOURCES += \
    main.cpp \
    systraybattery.cpp \
    journal.cpp \
    dialoglog.cpp

linux {
SOURCES += libhid/lib/hid.c
}

HEADERS += \
    systraybattery.h \
    journal.h \
    dialoglog.h \
    eventfilter.h

linux {
HEADERS += libhid/include/hidapi.h
}

FORMS += \
    systraybattery.ui \
    dialoglog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icons.qrc
