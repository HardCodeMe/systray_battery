#include "dialoglog.h"
#include "ui_dialoglog.h"
#include <QProgressDialog>

DialogLog::DialogLog(journal &jrnl, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogLog),
    jrnl(jrnl)
{
    ui->setupUi(this);

    ui->setupUi(this);
    this->setWindowTitle("Log data");

    this->createUI(jrnl);
}

DialogLog::~DialogLog()
{
    delete ui;
}

void DialogLog::createUI(journal &jrnl)
{
    quint32 lines;
    journal::jline_t jline;

    QStringList header = {"№ записи", "Дата", "Время", "Ext PWR", "Charger1", "Battery1", "Charger2", "Battery2"};

    /* Указываем число колонок */
    ui->tableWidget->setColumnCount(header.count());

    /* Включаем сетку */
    ui->tableWidget->setShowGrid(true);

    /* Разрешаем выделение только одного элемента */
    ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

    /* Разрешаем выделение построчно */
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

    /* Устанавливаем заголовки колонок */
    ui->tableWidget->setHorizontalHeaderLabels(header);

    /* Скрываем колонку под номером 0 */
    ui->tableWidget->verticalHeader()->hide();

    /* Раятягиваем последнюю колонку на всё доступное пространство */
    //ui->tableWidget->horizontalHeader()->setStretchLastSection(true);

    /* Заполняем QTableWidget */
    lines = jrnl.get_lines();

    QProgressDialog *pprd = new QProgressDialog("Чтение журнала ...", "&Cancel", 0, lines);
    pprd->setMinimumDuration(0);
    pprd->setWindowTitle("Please Wait");

    for (quint32 i = 0; i < lines; i++) {

        /* Считываем строку из файла */
        jrnl.read_line(i, jline);

        /* Вставляем строку */
        ui->tableWidget->insertRow((i * 9) + 0);
        ui->tableWidget->insertRow((i * 9) + 1);
        ui->tableWidget->insertRow((i * 9) + 2);
        ui->tableWidget->insertRow((i * 9) + 3);
        ui->tableWidget->insertRow((i * 9) + 4);
        ui->tableWidget->insertRow((i * 9) + 5);
        ui->tableWidget->insertRow((i * 9) + 6);
        ui->tableWidget->insertRow((i * 9) + 7);
        ui->tableWidget->insertRow((i * 9) + 8);

        ui->tableWidget->setSpan((i * 9) + 0, 0, 9, 1);
        ui->tableWidget->setSpan((i * 9) + 0, 1, 9, 1);
        ui->tableWidget->setSpan((i * 9) + 0, 2, 9, 1);

#define BYTEBCDTOHEX(bcdbyte) \
    (((bcdbyte) & 0xf) + (((bcdbyte) >> 4) & 0xf) * 10)

#define BYTEHEXTOBCD(bcdbyte) \
    (((((bcdbyte) / 10) << 4) & 0xf0) + (((bcdbyte) % 10) & 0xf))

        ui->tableWidget->setItem((i * 9) + 0, 0, new QTableWidgetItem(QString::number(i)));
        ui->tableWidget->setItem((i * 9) + 0, 1, new QTableWidgetItem(QString::number(BYTEBCDTOHEX(jline.datetime.day)).rightJustified(2, '0') + "." + QString::number(BYTEBCDTOHEX(jline.datetime.month)).rightJustified(2, '0') + "." + QString::number(BYTEBCDTOHEX(jline.datetime.year)).rightJustified(2, '0')));
        ui->tableWidget->setItem((i * 9) + 0, 2, new QTableWidgetItem(QString::number(BYTEBCDTOHEX(jline.datetime.hour)).rightJustified(2, '0') + ":" + QString::number(BYTEBCDTOHEX(jline.datetime.min)).rightJustified(2, '0') + ":" + QString::number(BYTEBCDTOHEX(jline.datetime.sec)).rightJustified(2, '0')));

        ui->tableWidget->setItem((i * 9) + 0, 3, new QTableWidgetItem(QString("Vin: " + QString::number(jline.pwrsrc.Vin))));
        ui->tableWidget->setItem((i * 9) + 1, 3, new QTableWidgetItem(QString("Iin: " + QString::number(jline.pwrsrc.Iin))));

        ui->tableWidget->setItem((i * 9) + 0, 4, new QTableWidgetItem(QString("Vbat: " + QString::number(jline.charger1.Vbat))));
        ui->tableWidget->setItem((i * 9) + 1, 4, new QTableWidgetItem(QString("Vin: " + QString::number(jline.charger1.Vin))));
        ui->tableWidget->setItem((i * 9) + 2, 4, new QTableWidgetItem(QString("Vout: " + QString::number(jline.charger1.Vout))));
        ui->tableWidget->setItem((i * 9) + 3, 4, new QTableWidgetItem(QString("Iin: " + QString::number(jline.charger1.Iin))));
        ui->tableWidget->setItem((i * 9) + 4, 4, new QTableWidgetItem(QString("Ibat: " + QString::number(jline.charger1.Ibat))));
        ui->tableWidget->setItem((i * 9) + 5, 4, new QTableWidgetItem(QString("temp: " + QString::number(jline.charger1.die_temp))));
        ui->tableWidget->setItem((i * 9) + 6, 4, new QTableWidgetItem(QString("Bat temp: " + QString::number(jline.charger1.bat_temp))));

        ui->tableWidget->setItem((i * 9) + 0, 5, new QTableWidgetItem(QString("Vcell1: " + QString::number(jline.monitor1.cell_voltage[0]))));
        ui->tableWidget->setItem((i * 9) + 1, 5, new QTableWidgetItem(QString("Vcell2: " + QString::number(jline.monitor1.cell_voltage[1]))));
        ui->tableWidget->setItem((i * 9) + 2, 5, new QTableWidgetItem(QString("Vcell3: " + QString::number(jline.monitor1.cell_voltage[2]))));
        ui->tableWidget->setItem((i * 9) + 3, 5, new QTableWidgetItem(QString("Vcell4: " + QString::number(jline.monitor1.cell_voltage[3]))));
        ui->tableWidget->setItem((i * 9) + 4, 5, new QTableWidgetItem(QString("Vcell5: " + QString::number(jline.monitor1.cell_voltage[4]))));
        ui->tableWidget->setItem((i * 9) + 5, 5, new QTableWidgetItem(QString("Vcell6: " + QString::number(jline.monitor1.cell_voltage[5]))));
        ui->tableWidget->setItem((i * 9) + 6, 5, new QTableWidgetItem(QString("MonTemp: " + QString::number(jline.monitor1.temperature))));
        ui->tableWidget->setItem((i * 9) + 7, 5, new QTableWidgetItem(QString("Qoul: " + QString::number(jline.monitor1.qoulombs))));
        ui->tableWidget->setItem((i * 9) + 8, 5, new QTableWidgetItem(QString("Qoulmax: " + QString::number(jline.monitor1.qoulombs_max))));

        ui->tableWidget->setItem((i * 9) + 0, 6, new QTableWidgetItem(QString("Vbat: " + QString::number(jline.charger2.Vbat))));
        ui->tableWidget->setItem((i * 9) + 1, 6, new QTableWidgetItem(QString("Vin: " + QString::number(jline.charger2.Vin))));
        ui->tableWidget->setItem((i * 9) + 2, 6, new QTableWidgetItem(QString("Vout: " + QString::number(jline.charger2.Vout))));
        ui->tableWidget->setItem((i * 9) + 3, 6, new QTableWidgetItem(QString("Iin: " + QString::number(jline.charger2.Iin))));
        ui->tableWidget->setItem((i * 9) + 4, 6, new QTableWidgetItem(QString("Ibat: " + QString::number(jline.charger2.Ibat))));
        ui->tableWidget->setItem((i * 9) + 5, 6, new QTableWidgetItem(QString("temp: " + QString::number(jline.charger2.die_temp))));
        ui->tableWidget->setItem((i * 9) + 6, 6, new QTableWidgetItem(QString("Bat temp: " + QString::number(jline.charger2.bat_temp))));

        ui->tableWidget->setItem((i * 9) + 0, 7, new QTableWidgetItem(QString("Vcell1: " + QString::number(jline.monitor2.cell_voltage[0]))));
        ui->tableWidget->setItem((i * 9) + 1, 7, new QTableWidgetItem(QString("Vcell2: " + QString::number(jline.monitor2.cell_voltage[1]))));
        ui->tableWidget->setItem((i * 9) + 2, 7, new QTableWidgetItem(QString("Vcell3: " + QString::number(jline.monitor2.cell_voltage[2]))));
        ui->tableWidget->setItem((i * 9) + 3, 7, new QTableWidgetItem(QString("Vcell4: " + QString::number(jline.monitor2.cell_voltage[3]))));
        ui->tableWidget->setItem((i * 9) + 4, 7, new QTableWidgetItem(QString("Vcell5: " + QString::number(jline.monitor2.cell_voltage[4]))));
        ui->tableWidget->setItem((i * 9) + 5, 7, new QTableWidgetItem(QString("Vcell6: " + QString::number(jline.monitor2.cell_voltage[5]))));
        ui->tableWidget->setItem((i * 9) + 6, 7, new QTableWidgetItem(QString("MonTemp: " + QString::number(jline.monitor2.temperature))));
        ui->tableWidget->setItem((i * 9) + 7, 7, new QTableWidgetItem(QString("Qoul: " + QString::number(jline.monitor2.qoulombs))));
        ui->tableWidget->setItem((i * 9) + 8, 7, new QTableWidgetItem(QString("Qoulmax: " + QString::number(jline.monitor2.qoulombs_max))));

        pprd->setValue(i);
        qApp->processEvents();
        if (pprd->wasCanceled())
        {
             break;
        }
    }

    pprd->setValue(lines) ;
    delete pprd;

    /* Ресайзим колонки по содержимому */
    ui->tableWidget->resizeColumnsToContents();
    //ui->tableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

void DialogLog::on_pushButton_clicked()
{
    jrnl.clearall();
    ui->tableWidget->clearContents();
}
