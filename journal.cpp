#include "journal.h"

#include <QDateTime>


journal::journal(quint32 maxlines):
    maxlines(maxlines)
{
    QString dir = QDir::current().currentPath();

    logfile.setFileName(dir + QDir::separator() + "log.bin");
    if (logfile.open(QFile::ReadWrite)) {
        if (!logfile.size()) {
            idx = 0;
            lines = 0;
            logfile.write((char *)&idx, sizeof(idx));
            while(!logfile.flush());
         }
        else {
            logfile.read((char *)&idx, sizeof(idx));
            lines = (logfile.size() - sizeof(idx)) / sizeof(jline_t);
        }
    }
}

journal::~journal()
{
    if (logfile.isOpen()) {
        logfile.close();
    }
}

bool journal::write_line(journal::jline_t &jline)
{
    if (logfile.isOpen()) {
        if (lines < maxlines) {
            logfile.seek(logfile.size());
        } else {
            logfile.seek((idx * sizeof(jline_t)) + sizeof(idx));
        }
        if (logfile.write((char *)&jline, sizeof(journal::jline_t)) == sizeof(jline)) {
            while(!logfile.flush());
            if (lines < maxlines) {
                lines++;
            } else {
                idx = (idx == (maxlines - 1)) ? 0 : (idx + 1);
                logfile.seek(0);
                logfile.write((char *)&idx, sizeof(idx));
                while(!logfile.flush());
            }
            return true;
        }
    }

    return false;
}

quint32 journal::get_lines()
{
    return lines;
}

bool journal::read_line(uint32_t nline, journal::jline_t &jline)
{
    qint64 offset;

    offset = (((((idx + nline)) > (maxlines - 1)) ? ((idx + nline) - maxlines) :
                                                    (idx + nline)
                                                    ) *
                                                    sizeof(jline)) + sizeof(idx);

    if (logfile.isOpen() && (offset <= (logfile.size() - sizeof(jline)))) {
        logfile.seek(offset);
        if (logfile.read((char *)&jline, sizeof(jline)) == sizeof(jline)) {
            return true;
        }
    }

    return false;
}

bool journal::clearall()
{
    logfile.resize(0);
    logfile.flush();
}


