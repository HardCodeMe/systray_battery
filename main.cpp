#include "systraybattery.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SysTrayBattery w;

    QApplication::setQuitOnLastWindowClosed(false);

    return a.exec();
}
