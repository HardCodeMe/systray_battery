#ifndef DIALOGLOG_H
#define DIALOGLOG_H

#include <QDialog>
#include "journal.h"


namespace Ui {
class DialogLog;
}

class DialogLog : public QDialog
{
    Q_OBJECT

public:
    DialogLog(journal &jrnl, QWidget *parent = nullptr);
    ~DialogLog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::DialogLog *ui;
    journal &jrnl;

    void createUI(journal &jrnl);
};

#endif // DIALOGLOG_H
